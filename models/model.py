# vim: set tabstop=4 shiftwidth=4 expandtab smartindent:
import torch

from torch import nn
from torch.autograd import Variable

from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


class GenModel(nn.Module):
    def __init__(self, phoncount, args, tie_weights=False):
        super(GenModel, self).__init__()
        self.phoncount = phoncount
        self.rnns = nn.ModuleList([
            nn.LSTM(input_size=40, hidden_size=args.hidden_dim, bidirectional=True),
            nn.LSTM(input_size=2*args.hidden_dim, hidden_size=args.hidden_dim, bidirectional=True),
            nn.LSTM(input_size=2*args.hidden_dim, hidden_size=args.hidden_dim, bidirectional=True),
            nn.LSTM(input_size=2*args.hidden_dim, hidden_size=args.hidden_dim, bidirectional=True)])
        self.projection = nn.Sequential(nn.Linear(in_features=2*args.hidden_dim, out_features=250),
                                        nn.Dropout(),
                                        nn.ReLU(),
                                        nn.Linear(in_features=250, out_features=phoncount+1))
        
        self.init_weights()

    def forward(self, input, lens, forward=0, stochastic=False):
        h = input  # (n, t)
        h = pack_padded_sequence(h, lens.cpu().data.numpy())
        #h = self.lock_drop(h, 0.3)
        states = []
        for rnn in self.rnns:
            h, state = rnn(h)
            #h = self.lock_drop(h, 0.4)
            states.append(state)

        #h = self.lock_drop(h, 0.3)
        h, lens = pad_packed_sequence(h)
        h = self.projection(h)

        return h, lens

    def apply_dropout_embedding(x):
        if not self.training and not self.dropout:
            return
        pass

    # https://github.com/salesforce/awd-lstm-lm/blob/dfd3cb0235d2caf2847a4d53e1cbd495b781b5d2/locked_dropout.py#L5
    def lock_drop(self, x, d):
        if not self.training:
            return x
        m = x.data.new(1, x.size(1), x.size(2)).bernoulli_(1 - d)
        mask = Variable(m, requires_grad=False) / (1 - d)
        mask = mask.expand_as(x)
        return mask * x

    def init_weights(self):
        initrange = 0.1
        self.projection[0].bias.data.fill_(0)
        self.projection[0].weight.data.uniform_(-initrange, initrange)
        self.projection[3].bias.data.fill_(0)
        self.projection[3].weight.data.uniform_(-initrange, initrange)




class WeightDropped(nn.GRU):

    def __init__(self, dropout=0.5, *args, **kwargs):
        super(WeightDropped, self).__init__(**kwargs)
        self.old_weight_hh_l0 = self.weight_hh_l0
        self.weight_hh_l0 = None
        del self._parameters['weight_hh_l0']

    def flatten_parameters(self):
        self._data_ptrs = []

    def dropout_layer(self, x):
        mask = x.data.new().resize_(x.size()).fill_(0).bernoulli_(1 - self.dropout)
        mask = Variable(mask, requires_grad=False)
        mask = mask / (1 - self.dropout)
        x = x * mask
        return x

    def forward(self, input, hx=None):
        if self.training:
            self.weight_hh_l0 = self.dropout_layer(self.old_weight_hh_l0)
        else:
            self.weight_hh_l0 = self.old_weight_hh_l0
        return super(WeightDropped, self).forward(input, hx=hx)


def generate(model, sequence_length, batch_size, args, stochastic=False, inp=None):
    if inp is None:
        inp = Variable(torch.zeros(batch_size, 1)).long()
        if args.cuda:
            inp = inp.cuda()
    model.eval()
    logits = model(inp, forward=sequence_length, stochastic=stochastic)
    classes = torch.max(logits, dim=2)[1]
    return classes
