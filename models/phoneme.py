import csv
import os
import numpy as np
import random
import torch

from inferno.trainers.basic import Trainer
from inferno.extensions.metrics.categorical import CategoricalError
from inferno.trainers.callbacks.base import Callback

from warpctc_pytorch import CTCLoss
from torch.utils.data import DataLoader, Dataset
from collections import namedtuple
from torch import nn
from torch.nn.utils.rnn import pad_packed_sequence, pack_padded_sequence
from torch.autograd import Variable

from model import GenModel
from phoneme_list import *
from torch.nn import functional as F
from ctcdecode import CTCBeamDecoder

# Configure arguments
args = namedtuple('args',
                  [
                      'batch_size',
                      'save_directory',
                      'epochs', 
                      'cuda',
                      'embedding_dim',
                      'hidden_dim'])(
    16,
    'output1/phoneme',
    12,
    True,
    128,
    400)

def pathify(x):
    dirn = os.path.dirname(os.path.abspath(__file__))
    return os.path.abspath(os.path.join(dirn, x))


class PhonemeDataset(Dataset):
    
    def __init__(self, data, labels, *args, **kwargs):
        self.data = data
        self.labels = labels
    
    def __len__(self):
        return self.data.shape[0]
    
    def __getitem__(self, idx):
        return (torch.from_numpy(self.data[idx]), torch.from_numpy(self.labels[idx]))

    
def collate_batch(batch):
    data, targets = zip(*batch)
    data = sorted(zip(data, range(len(list(data)[0]))), key=lambda x: x[0].shape[0], reverse=True)

    data, p = zip(*data)

    new_targets = []
    for j in p:
        new_targets.append(targets[j])

    targets = new_targets

    lengths = [len(x) for x in data]
    phon_lens = [len(x) for x in targets]

    max_len = max(lengths)
    max_phon = max(phon_lens)
    
    inp = np.zeros([len(batch), max_len, 40])
    for i, x in enumerate(data):
        inp[i, 0:len(x), :] = x
        
#     targets = np.zeros([len(batch), max_phon, 40])
#     for i, x in enumerate(targets):
#         targets[i, :len(x)] = x
    targets = [np.array(x, dtype=np.int32) for x in targets]
    targets = torch.from_numpy(np.concatenate(targets))
    targets = targets + 1

    lengths = torch.from_numpy(np.array(lengths, dtype=np.int32))
    phon_lengths = torch.from_numpy(np.array(phon_lens, dtype=np.int32))
    
    inp = torch.from_numpy(inp)
    inp = inp.transpose(0, 1)
    

    return (inp, lengths, targets, phon_lengths)


class CTCLoss(CTCLoss):
    def forward(self, input, target):
        return super(CTCLoss, self).forward(input[0], target[0].cpu(), Variable(torch.IntTensor(input[1]), requires_grad=False), target[1].cpu())

class CustomLogger(Callback):
    def end_of_training_iteration(self, **_):
        training_loss = self.trainer.get_state('training_loss', default=0)
        print("Training loss: {}".format(training_loss.numpy()[0] / args.batch_size))

def train_model(model, args):
    model = model.cuda() if args.cuda else model
    kw = {'num_workers': 2, 'pin_memory': True} if args.cuda else {}
    train_data = np.load('train.npy')
    train_labels = np.load('train_phonemes.npy')
    dataset = PhonemeDataset(train_data, train_labels)
    loader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True, collate_fn=collate_batch, **kw)
    criterion = CTCLoss().cuda() if args.cuda else CTCLoss()
    trainer = Trainer(model) \
        .build_criterion(criterion) \
        .build_optimizer('Adam') \
        .save_every((1, 'epochs')) \
        .save_to_directory(args.save_directory) \
        .set_max_num_epochs(args.epochs) \
        .register_callback(CustomLogger) \
        .bind_loader('train', loader, num_inputs=2, num_targets=2)

    if args.cuda:
        trainer.cuda()

    # Go!
    trainer.fit()

    trainer.save(args.save_directory)
    torch.save(trainer.model, 'ch.pth')


def load_and_eval():

    t = Trainer().load('output1/phoneme')
    model = t.model

    label_map = [' '] + PHONEME_MAP
    decoder = CTCBeamDecoder(
        labels=label_map,
        blank_id=0
    )
    
    test_data = np.load('test.npy')
    dataset = PhonemeDataset(test_data, [np.array((1, 1))] * test_data.shape[0])
    loader = torch.utils.data.DataLoader(dataset, collate_fn=collate_batch, batch_size=1, shuffle=False)
    out = []
    model = model.cuda()
    model.eval()
    for b in loader:
        logits = model(Variable(b[0]).float().cuda(), Variable(b[1]).cuda())
        lens = torch.IntTensor(logits[1]).cuda()
        logits = torch.transpose(logits[0], 0, 1)
        probs = F.softmax(logits, dim=2).data.cpu()
        output, scores, timesteps, out_seq_len = decoder.decode(probs=probs, seq_lens=lens)
        
        for i in range(output.size(0)):
            chrs = "".join(label_map[o] for o in output[i, 0, :out_seq_len[i, 0]])
            out.append(chrs) 

    curr_i = 0
    with open('preds.txt', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(('Id', 'Pred'))
        for o in out:
            writer.writerow((curr_i, o))
            curr_i += 1 
        

if __name__ == '__main__':
    model = GenModel(46, args)
    #train_model(model, args)
    load_and_eval()
